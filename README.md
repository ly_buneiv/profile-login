# Readme

Profile Login Module.

## Installation

You can install the package using Composer:

```bash
composer require lybuneiv/profilelogin
```

Then add the service provider to config/app.php:

```bash
Lybuneiv\Profilelogin\ProfileloginServiceProvider::class,
```
Then add the aliases service provider to config/app.php:

```bash
'Profilelogin' => Lybuneiv\Profilelogin\Facades\ProfileloginFacade::class
```

Publish the all file:

```bash
php artisan vendor:publish --provider="Lybuneiv\Profilelogin\ProfileloginServiceProvider" --force
```


## Usage

First need to create account and domain in app customer profile login to get app key and secret

Add in env file

```bash
PROFILE_APP_URL=http://laravel8.home
PROFILE_APP_LOGIN_URL=http://laravel8.home/app/login
PROFILE_APP_KEY=850d33c2360c793c766876ff928de380
PROFILE_APP_SECRET=EMtO3o403aba1e8f5b4217fca9ee19acc9de59
```

Url for profile login app

```bash
{!! Profilelogin::url() !!}
```

Check and get email validation token with app by request (t_id and t_token)

```bash
{!! Profilelogin::getEmailValidationToken($request); !!}
```
