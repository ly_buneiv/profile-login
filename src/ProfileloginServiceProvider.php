<?php

namespace Lybuneiv\Profilelogin;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Lybuneiv\Profilelogin\Facades\Profilelogin;
use Illuminate\Routing\Router;

class ProfileloginServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishAll();

	}
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('profilelogin', function($app) {
	      return new Profilelogin();
	  	});
	}


	protected function publishAll()
    {
        $this->publishes([
            $this->packagePath('config/config.php') => config_path('profilelogin.php'),
        ], 'config');
    }

    /**
     * Loads a path relative to the package base directory
     *
     * @param string $path
     * @return string
     */
    protected function packagePath($path = '')
    {
        return sprintf("%s/../%s", __DIR__ , $path);
    }
}