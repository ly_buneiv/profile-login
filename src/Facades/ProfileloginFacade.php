<?php

namespace Lybuneiv\Profilelogin\Facades;

use Illuminate\Support\Facades\Facade;

class ProfileloginFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'profilelogin';
    }
}
