<?php

namespace Lybuneiv\Profilelogin\Facades;
use GuzzleHttp\Client;

class Profilelogin
{

    /**
     * Render login url
     * @return view as string
     */
    public function url()
    {
        $app_key = config('profilelogin.app_key');
        $app_secret = config('profilelogin.app_secret');
        $app_login_url = config('profilelogin.app_url').'/app/login';
        if($app_login_url && $app_key && $app_secret){
            $param = [
                'app_key' => $app_key,
                'app_secret' => $app_secret,
                'current_url' => url()->full()
            ];
            $param_str = http_build_query($param);
            return $app_login_url.'?'.$param_str;
        }
    }

    public function getEmailValidationToken($request)
    {
        $check_validation_url = config('profilelogin.app_url');
        if($request->t_id && $request->t_key && $check_validation_url){

            $check_validation_url = $check_validation_url.'/api/verify-token';
            $httpClient = new Client();

            try {
                $response = $httpClient->request('POST', $check_validation_url, [
                    'form_params' => [
                        't_id' => $request->t_id ?? '',
                        't_key' => $request->t_key ?? ''
                    ]
                ]);
                $code = $response->getStatusCode();
                if($code == 200){
                    $json = json_decode((string)$response->getBody());
                    return $json;
                }

            } catch (Exception $exc) {
                try_on_catch($exc);
                return '';
            }
        }

        return '';

    }

}
