<?php

return [
	'app_url' => env('PROFILE_APP_URL'),
	'app_login_url' => env('PROFILE_APP_LOGIN_URL'),
    'app_key' => env('PROFILE_APP_KEY'),
    'app_secret' => env('PROFILE_APP_SECRET')
];
